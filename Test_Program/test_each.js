const each = require('../problems/each');

const items = require('../problems/details');

each(items, (element, index) => {
    console.log(`Element at index ${index}: ${element}`);
});
