
const details = require("../problems/details");
const filter = require("../problems/filter");

const filteredArray = filter(details, element => element > 1);
console.log("Filtered Array:", filteredArray);
